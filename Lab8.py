# 1)именовка x у 2) Именовка рисунков снизу Рис 3 - кол-во найденных точек 4) То что у меня не торенд. Вывести график с линией. 5) Распределить данные линейно, перестроить график, не правильный, прааболичеаский, не прямой. 6) Установить графи, показать саму функцию на гарфике и формулу вычисленичя
import matplotlib.pyplot as plt
import matplotlib.patches as pch
import random
import time

def in_area(dot, center, rad):
    return pow((center[0]-dot[0]), 2) + pow((center[1]-dot[1]), 2) <= rad**2

def search(points, center, rad):
    count = 0
    for point in points:
        if in_area(point, center, rad):
            count += 1
    return count


num_data = []
time_data = []
num_repeat = int(10e3)
all_time = 0



try:
    number = int(input('Количество точек : '))
    rad = float(input('Радиус : '))

except ValueError:
    print('Ошибка при вводе значений радиуса/количества точек')

coordinates = [(random.uniform(-10, 10), random.uniform(-10, 10)) for i in range(number)]
center = (random.uniform(-10,10), random.uniform(-10, 10))

print(f'Точек : {search(coordinates, center, rad)}')

ax = plt.subplot()
ax.scatter(center[0], center[1], c='red', s=11, zorder=100, marker='X')
ax.scatter([x for x, y in coordinates if in_area((x, y), center, rad) is True],
           [y for x, y in coordinates if in_area((x, y), center, rad) is True],
           s=9,
           c='blue',
           zorder=50,
           marker='p')
ax.scatter([x for x, y in coordinates if in_area((x, y), center, rad) is False],
           [y for x, y in coordinates if in_area((x, y), center, rad) is False],
           s=3,
           c='red',
           zorder=0)

ax.add_patch(pch.Circle(center, radius=rad, fill=False))
ax.axis('square')

plt.show()

for i in range(1, 11):
    for n in range(3):
        coordinates = [(random.uniform(-10, 10), random.uniform(-10, 10)) for point in range(num_repeat)]
        center = (random.uniform(-10, 10), random.uniform(-10, 10))
        radius = random.uniform(-10, 10)
        start = time.time()
        count = search(coordinates, center, radius)
        stop = time.time()
        all_time += (stop - start)
    coordinates = []
    time_data.append(all_time/3)
    num_data.append(num_repeat)
    print(all_time/3)

    file = open("program_time.txt", 'a')
    file.write(f'{(all_time / 3):.1f}\n')
    num_repeat += int(100e3) * i

plt.plot(num_data, time_data, 'o')
plt.plot(num_data, time_data, 'b', c='red')
plt.grid()
plt.title('График тренда')
plt.xlabel('x')
plt.ylabel('y')
plt.show()
